# == Class: pgbouncer::params
#
# Parameters to be set for pgbouncer
#
# === Copyright
#
# GPL-3.0+
#
class pgbouncer::params {
  case $::osfamily {
    'redhat': {
      $log_dir     = '/var/log/pgbouncer'
      $run_dir     = '/var/run/pgbouncer'
      $owner_user  = 'pgbouncer'
      $owner_group = 'pgbouncer'
    }
    'debian': {
      # do nothing
      $log_dir     = '/var/log/postgresql'
      $run_dir     = '/run/postgresql'
      $owner_user  = 'postgres'
      $owner_group = 'postgres'
    }
    default: {
      fail("Unsupported osfamily ${::osfamily}")
    }
  }
  $logfile     = "${log_dir}/pgbouncer.log"
  $pidfile     = "${run_dir}/pgbouncer.pid"
}
